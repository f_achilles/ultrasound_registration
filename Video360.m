% make movie 360 degrees

VidObj = VideoWriter('Video360Degrees','MPEG-4');
VidObj.FrameRate=30;
open(VidObj)
axis vis3d image off
view(3)
[az,el]=view;
set(gcf,'RendererMode','manual','Renderer','zbuffer');
for viewAngle=1:1:360
    view([az+viewAngle,el]);
    vol3d(h);
    drawnow
    currFrame = getframe(gcf);
    writeVideo(VidObj,currFrame);
end
close(VidObj)