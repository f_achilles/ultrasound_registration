% This script creates a wire phantom geometry
wirePhantom = [];

%% parameters

% Spatial resolution along a wire in [mm]
sRes = 0.5;

% x and z coordinates of the wire endings on side 1 in [mm]
%  ^  o
%  |       o
% z| o     o
%  |________>
%       x
% layer 1
s1{1} = [35 0];
s1{2} = [55 0];
s1{3} = [60 0];
% layer 2
s1{4} = [30 10];
s1{5} = [35 10];
s1{6} = [60 10];
% layer 3
s1{7} = [30 20];
s1{8} = [55 20];
s1{9} = [60 20];

% x and z coordinates of the wire endings on side 2 in [mm]
% layer 1
s2{1} = [35 0];
s2{2} = [40 0];
s2{3} = [60 0];
% layer 2
s2{4} = [30 10];
s2{5} = [55 10];
s2{6} = [60 10];
% layer 3
s2{7} = [30 20];
s2{8} = [35 20];
s2{9} = [60 20];

% distance between side 1 and side 2 in [mm]
dist = 40;

%% phantom generation
% for each wire, run along the line connecting the end points
for i = 1:numel(s1)
    cStuetzpunkt = [s1{i}(1) 0 s1{i}(2)];
    cGoal = [s2{i}(1) dist s2{i}(2)];
    cDirVec = cGoal - cStuetzpunkt;
    cDirVec = cDirVec/norm(cDirVec);
    cWirePt = cStuetzpunkt + cDirVec*sRes;
    while norm(cStuetzpunkt-cWirePt) < norm(cStuetzpunkt-cGoal)
        wirePhantom = [wirePhantom; cWirePt];
        cWirePt = cWirePt + cDirVec*sRes;
    end
end

% plot the resulting wire phantom
figure; plot3(wirePhantom(:,1),wirePhantom(:,2),wirePhantom(:,3),'m.')
axis vis3d image