%% This script reads in a volume and converts it to a pointcloud after
%% thresholding.
% clear all
%% parameters
thr = 40;

%% read in data
% filename = '..\ValidationVolumes\TrackedImageSequence_CaptureDevice_20140506_144130_volume_aurora.mha';
[V,info]=ReadData3D(filename);
offset=info.Offset;
%% remove big artifacts
% leave only objects that are bigger than 5x5x5mm
dilateCube = strel(ones(3,3,3));
Vbin=imdilate(V>thr,dilateCube);
erodeCube = strel(ones(5,5,5));
bigDilateCube = strel(ones(12,12,12));
smallDilateCube = strel(ones(2,2,2));
Vbin=imdilate(imerode(Vbin,erodeCube),bigDilateCube);
%% threshold and create pointcloud
subtrPts = find(Vbin);
[xP, yP, zP]=ind2sub(size(V),subtrPts);
clear subtrPts
xP=single(xP/2)+offset(1)-0.5;
yP=single(yP/2)+offset(2)-0.5;
zP=single(zP/2)+offset(3)-0.5;
volFig=figure;


%% simple point cloud view
plot3(xP, yP, zP, 'r.')
axis vis3d image
view(3)


Vbin=(V>thr) & ~(Vbin);

% close wire parts that are not connected
Vbin=imclose(Vbin,erodeCube);
% Vbin=imerode(Vbin,smallDilateCube);


%% threshold and create pointcloud
nonZeroPtsPolaris = find(Vbin);
% clear Vbin
[xP, yP, zP]=ind2sub(size(V),nonZeroPtsPolaris);
xP=single(xP/2)+offset(1)-0.5;
yP=single(yP/2)+offset(2)-0.5;
zP=single(zP/2)+offset(3)-0.5;

%% simple point cloud view
hold on
plot3(xP, yP, zP, '.');
hold off
axis vis3d image
view(3)

