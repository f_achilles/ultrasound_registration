% This script reads in a volume, converts it to a point cloud and fits a
% geometric model to it.
close all

ReadAndDisplayVolume

MakeWirePhantom

% choose centric points from the US pointcloud
numPhantPts = size(wirePhantom,1);
xC = (min(xP)+max(xP))/2;
yC = (min(yP)+max(yP))/2;
zC = (min(zP)+max(zP))/2;
% calc distance from the center for each point
dist = sqrt((xP-xC).^2+(yP-yC).^2+(zP-zC).^2);
[~,sortedIdx]=sort(dist);
sortedIdx = sortedIdx(1:numPhantPts);

% provide initial fit with Horns method
[~, Hfit, Herr] = absor(wirePhantom', [xP(sortedIdx)';yP(sortedIdx)';zP(sortedIdx)']);
figure(volFig)
hold on
HornFitObj = plot3(Hfit(1,:),Hfit(2,:),Hfit(3,:),'g.');
hold off
wirePhantom = Hfit';

% find principal axes
[~,~,V]=svd([xP yP zP]);
hold on
plot3([xC; xC+V(1,1)*20], [yC; yC+V(1,2)*20],[zC; zC+V(1,3)*20],'color','red','linewidth',4)
plot3([xC; xC+V(2,1)*20], [yC; yC+V(2,2)*20],[zC; zC+V(2,3)*20],'color','green','linewidth',4)
plot3([xC; xC+V(3,1)*20], [yC; yC+V(3,2)*20],[zC; zC+V(3,3)*20],'color','blue','linewidth',4)
hold off

break
% improve fit with multiple ICP iterations
err = Herr.errlsq/numPhantPts; numWhile=0;
while err(end)>0.5 && numWhile<20
[TR, TT, err] = icp([xP';yP';zP'],wirePhantom');

wirePhantom = (TR*wirePhantom' + repmat(TT,1,size(wirePhantom,1)))';
numWhile=numWhile+1;
end
figure(volFig)
hold on
testObj=plot3(wirePhantom(:,1),wirePhantom(:,2),wirePhantom(:,3),'y.');
hold off