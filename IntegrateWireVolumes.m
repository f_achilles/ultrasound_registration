% This script fits a wire phantom to a compounded wire image and integrates
% voxel values along the wires in tube-shaped integration volumes.

%% parameters

% radius of integration tubes in [mm]
tubeRadius = 1.5;

%% run ICP

ICPPhantomToVolume

%% map wires to 3d grid

% 1mm maps to 2px
wirePhantom = wirePhantom*2;

% round to full indices
wirePhantom = round(wirePhantom);

% crop parts beyond US volume
[Sx, Sy, Sz] = size(V);
wirePhantom = wirePhantom(...
    wirePhantom(:,2)>0 & wirePhantom(:,2)<=Sx &...
    wirePhantom(:,1)>0 & wirePhantom(:,1)<=Sy &...
    wirePhantom(:,3)>0 & wirePhantom(:,3)<=Sz...
    ,:);

% make 3D grid, fill with true for wire
wireGrid = false(Sx, Sy, Sz);
wireGrid(sub2ind([Sx, Sy, Sz],wirePhantom(:,2),wirePhantom(:,1),wirePhantom(:,3))) = true;

% visualize for error checking
figure;
[xP, yP, zP]=ind2sub(size(V),nonZeroPtsPolaris);
plot3(xP, yP, zP, '.')
axis vis3d image
view(3)
hold on
isosurface(wireGrid)
hold off

% create tube-like wires by image dilation
radiusInPixels = tubeRadius/0.5;
[x,y,z] = ndgrid(-radiusInPixels:radiusInPixels);
sphereStruct = strel(sqrt(x.^2 + y.^2 + z.^2) <=(radiusInPixels+0.3));
wireGridDil = imdilate(wireGrid,sphereStruct);

% plot for error checking
figure;
isosurface(wireGridDil)
axis vis3d image
view(3)

% perform integration
wireIntegrationResult = sum(sum(sum(V(wireGridDil))))
file=dir(filename);
disp(['File was: ' file.name])
disp(['Residual error of ' num2str(err(end)) ', Number of ICP iterations: ' num2str(numWhile) '/10.'])


