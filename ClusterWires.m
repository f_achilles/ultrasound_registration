% This script looks to cluster nearby objects in the extracted pointcloud.

ReadAndDisplayVolume

% clear V
% ptCloud = single([xP, yP, zP]);
% 
% numPts = size(ptCloud,1);
% 
% [grid]=uint16(meshgrid(1:numPts));
% gridT=grid';
% return
% dist = sqrt(sum((ptCloud(grid(:),:)-ptCloud(gridT(:),:)).^2,2));
% clear ptCloud
% clear grid gridT
% dist=reshape(dist,numPts,numPts);
% dist(dist==0)=max(dist(:));
% [~,indices]=min(dist,[],1);
% clear dist
% % indices(i) is the index of the point closest to point i
% 
% connMatrix = eye(numPts);
% 
% indices=sub2ind([numPts numPts],indices,1:numPts);
% 
% connMatrix(indices)=1;
% clear indices
% tic
% timeWhile=toc;
% while timeWhile < 300
% connMatrixOld=connMatrix;
% connMatrix=connMatrix'*connMatrix;
% connMatrix(connMatrix>1)=1;
% if all(all(connMatrix==connMatrixOld))
%     disp(['Clustering done after ' num2str(i) ' interations.'])
%     break
% end
% timeWhile=toc;
% end
% 
% [groups,~,clusterIndices]=unique(connMatrix','rows');
% groups=logical(groups)';


CC = bwconncomp(Vbin,26);
numRegions=CC.NumObjects;
regions=CC.PixelIdxList;
clear CC
principalDirection = zeros(3,numRegions);
figure;
colOrdr=get(gca,'ColorOrder');
hold on
for i=1:numRegions
    [xP, yP, zP]=ind2sub(size(Vbin),regions{i});
    xP=single(xP/2);
    zP=single(zP/2);
    yP=single(yP/2);
    plot3(xP,yP,zP,'.','color',colOrdr(mod(i,size(colOrdr,1)-1)+1,:))
    [values, V] = pca([xP yP zP]);
    xC = (min(xP)+max(xP))/2;
    yC = (min(yP)+max(yP))/2;
    zC = (min(zP)+max(zP))/2;
    plot3([xC; xC+V(1,1)*20], [yC; yC+V(2,1)*20],[zC; zC+V(3,1)*20],'color','magenta','linewidth',4)
    principalDirection(:,i) = numel(xP)*V(:,1);
end
hold off
axis vis3d image
view(3)

% plot all weighted principal directions
figure;
hold on
for i=1:numRegions
    vec=principalDirection(:,i);
    plot3([0; vec(1,1)*20], [0; vec(2,1)*20],[0; vec(3,1)*20],'color',colOrdr(mod(i,size(colOrdr,1)-1)+1,:),'linewidth',4)
end
hold off
axis vis3d image
view(3)

% plot overall mean direction
meanDirection=mean(principalDirection,2);

[~,principPricipDir]= pca([principalDirection']);
principPricipDir = principPricipDir(:,1);
[xP, yP, zP]=ind2sub(size(Vbin),find(Vbin));
xP=single(xP/2);
zP=single(zP/2);
yP=single(yP/2);
xC = (min(xP)+max(xP))/2;
yC = (min(yP)+max(yP))/2;
zC = (min(zP)+max(zP))/2;
hold on
plot3([xC; xC+principPricipDir(1,1)*30], [yC; yC+principPricipDir(2,1)*30],[zC; zC+principPricipDir(3,1)*30],'color','black','linewidth',10)
hold off