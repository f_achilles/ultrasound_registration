%% This script reads in a volume and maps a wire model into it
directory='C:\Projects\TrackingPaper\ValidationVolumes';
DirContent=dir(directory);
pattern={'A*a*rora'};
Filenames={DirContent.name};
AuroraFiles=regexp(Filenames,pattern);
pattern={'P*p*olaris'};
PolarisFiles=regexp(Filenames,pattern);
AuroraFilenames={};
PolarisFilenames={};
for DirCount=1:numel(Filenames)
    if ~isempty(AuroraFiles{DirCount})
        AuroraFilenames = [AuroraFilenames Filenames{DirCount}];
    elseif ~isempty(PolarisFiles{DirCount})
        PolarisFilenames = [PolarisFilenames Filenames{DirCount}];
    end
end
wireVarianceResult=[];
for fileInd=1:10
filename = ['..\ValidationVolumes\' PolarisFilenames{fileInd}];
% start
close all

ReadAndDisplayVolume

MakeWirePhantom

file=dir(filename);
disp(['File is called: ' file.name])
trackingType= 'P';%input('Which Tracking was used? Type A(urora) or P(olaris): ');
switch trackingType
    case 'A'
        PhantomToReference = [...
        [[0.991845	0.0141111	0.126663];[0.126422	0.0167865	-0.991834];[-0.0161221	0.99976	0.0148657]] [34.4335;77.0468;-31.6522]; [0	0 0	1]...
        ];
    case 'P'
        PhantomToReference = [...
        -0.0516242	-0.0663189	0.996462	-122.814
        -0.00307913	-0.997777	-0.066566	43.7892
        0.998662	-0.00650465	0.0513052	45.9834
        0	0	0	1];
end
numWirePts = size(wirePhantom,1);
% default has lying vectors
wirePhantom = [wirePhantom'; ones(1,numWirePts)];
wirePhantom = PhantomToReference*wirePhantom;
wirePhantom = (wirePhantom(1:3,:))'; %should be in Reference frame now

% plot the transformed wire phantom
figure(volFig);
hold on
plot3(wirePhantom(:,1),wirePhantom(:,2),wirePhantom(:,3),'m.')
hold off
axis vis3d image

% convert to pixel coordinates
wirePhantom(:,1)=wirePhantom(:,1)-offset(1)+0.5;
wirePhantom(:,2)=wirePhantom(:,2)-offset(2)+0.5;
wirePhantom(:,3)=wirePhantom(:,3)-offset(3)+0.5;
wirePhantom = round(2*wirePhantom);

% crop parts beyond US volume
[Sx, Sy, Sz] = size(V);
wirePhantom = wirePhantom(...
    wirePhantom(:,1)>0 & wirePhantom(:,1)<=Sx &...
    wirePhantom(:,2)>0 & wirePhantom(:,2)<=Sy &...
    wirePhantom(:,3)>0 & wirePhantom(:,3)<=Sz...
    ,:);

% make 3D grid, fill with true for wire
wireGrid = false(Sx, Sy, Sz);
wireGrid(sub2ind([Sx, Sy, Sz],wirePhantom(:,1),wirePhantom(:,2),wirePhantom(:,3))) = true;

% create tube-like wires by image dilation, crop V
tubeRadius = 3; %[mm]
radiusInPixels = tubeRadius/0.5;
[x,y,z] = ndgrid(-radiusInPixels:radiusInPixels);
sphereStruct = strel(sqrt(x.^2 + y.^2 + z.^2) <=(radiusInPixels+0.3));
wireGridDil = imdilate(wireGrid,sphereStruct);
V = V.*wireGridDil;
delete wireGridDil
figure; h=vol3d('CData',V);
axis vis3d image off

%store new V for later visualization
gipl_write_volume(uint8(V),[file.name(1:end-4) 'Cropped'],info.PixelDimensions)
% img = ImageType(size(V),info.Offset,info.PixelDimensions,info.TransformMatrix);
% img.data = uint8(V);
% write_mhd([file.name(1:end-4) 'Cropped'],img);
break

% perform variance on along wires
wireIntensities=V(wireGrid);
wireVarianceResult = [wireVarianceResult var(wireIntensities)];

% plot intensities
figure;
wirePlot=plot(wireIntensities);
saveas(gcf,['PolarisWires' num2str(fileInd)],'png')
disp(['File was: ' file.name])
end
save('PolarisWirePlots','wireVarianceResult')